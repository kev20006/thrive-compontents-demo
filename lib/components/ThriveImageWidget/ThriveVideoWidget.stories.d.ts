import type { Meta, StoryObj } from "@storybook/react";
import ThriveVideoWidget from "./ThriveVideoWidget";
declare const meta: Meta<typeof ThriveVideoWidget>;
type Story = StoryObj<typeof ThriveVideoWidget>;
export declare const AllGood: Story;
export declare const VideoPlaybackError: Story;
export default meta;
