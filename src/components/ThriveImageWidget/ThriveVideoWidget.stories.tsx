import React from "react";
import type { Meta, StoryObj } from "@storybook/react";
import ThriveVideoWidget from "./ThriveVideoWidget";

const meta: Meta<typeof ThriveVideoWidget> = {
  title: "Components/ThriveVideoWidget",
  component: ThriveVideoWidget,
};

type Story = StoryObj<typeof ThriveVideoWidget>;

export const AllGood: Story = {
  args: {
    videoUrl: "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4",
    featuredImageUrl: "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/images/BigBuckBunny.jpg", 
    caption: 'Default Video Caption',
    description: 'default video description',
    firstPlayEvent: () => console.log('first time clicking play'),
    options: {style:{ maxWidth: '400px' }}
  },
};


export const VideoPlaybackError: Story = {
  args: {
    videoUrl: "fdgdfsgfds.mp4",
    featuredImageUrl: "dsfgdfsgdfsggfds.jpg", 
    description: 'default video description',
    cannotPlayError: {errorBody:'cannot play error body', errorTitle: 'cannot play error title'}
  },
};

export default meta;