import React from 'react';
import './ThriveVideoWidget.scss';
declare const Video: ({ videoUrl, featuredImageUrl, caption, description, firstPlayEvent, options, cannotPlayError, downloadOnError }: {
    videoUrl: string;
    featuredImageUrl?: string | undefined;
    caption?: string | undefined;
    description?: string | undefined;
    firstPlayEvent?: (() => void) | undefined;
    cannotPlayError?: {
        errorTitle: string;
        errorBody: string;
    } | undefined;
    options?: any;
    downloadOnError?: boolean | undefined;
}) => React.JSX.Element | null;
export default Video;
