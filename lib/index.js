'use strict';

var React = require('react');

/******************************************************************************
Copyright (c) Microsoft Corporation.

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
PERFORMANCE OF THIS SOFTWARE.
***************************************************************************** */
/* global Reflect, Promise, SuppressedError, Symbol */


var __assign = function() {
    __assign = Object.assign || function __assign(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p)) t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};

typeof SuppressedError === "function" ? SuppressedError : function (error, suppressed, message) {
    var e = new Error(message);
    return e.name = "SuppressedError", e.error = error, e.suppressed = suppressed, e;
};

var Video = function (_a) {
    var videoUrl = _a.videoUrl, featuredImageUrl = _a.featuredImageUrl, caption = _a.caption, description = _a.description, firstPlayEvent = _a.firstPlayEvent, _b = _a.options, options = _b === void 0 ? {} : _b, cannotPlayError = _a.cannotPlayError, downloadOnError = _a.downloadOnError;
    var _c = React.useState(true), firstPlay = _c[0], setFirstPlay = _c[1];
    var _d = React.useState(false), cannotPlay = _d[0], setCannotPlay = _d[1];
    if (!videoUrl) {
        return null;
    }
    return (React.createElement("div", __assign({ className: "widget-video ".concat(options.className) }, options),
        cannotPlay
            ? React.createElement("div", null,
                React.createElement("h3", null, cannotPlayError === null || cannotPlayError === void 0 ? void 0 : cannotPlayError.errorTitle),
                React.createElement("p", null, cannotPlayError === null || cannotPlayError === void 0 ? void 0 : cannotPlayError.errorBody),
                downloadOnError && React.createElement("a", { href: 'videoUrl', download: true }, "Download"))
            : React.createElement("video", { poster: featuredImageUrl, onPlaying: function () {
                    if (firstPlay) {
                        firstPlayEvent === null || firstPlayEvent === void 0 ? void 0 : firstPlayEvent();
                        setFirstPlay(false);
                    }
                }, controls: true, playsInline: true },
                React.createElement("source", { src: videoUrl, onError: function () { return setCannotPlay(true); } })),
        (caption || description) &&
            React.createElement("div", { className: "widget-caption" }, caption || description)));
};

exports.ThriveVideoWidget = Video;
//# sourceMappingURL=index.js.map
