import React, { useState } from 'react';

import './ThriveVideoWidget.scss';


const Video = ({videoUrl, featuredImageUrl, caption, description, firstPlayEvent, options={}, cannotPlayError, downloadOnError}:{videoUrl: string, featuredImageUrl?: string, caption?: string, description?: string, firstPlayEvent?: () => void, cannotPlayError?:{errorTitle: string, errorBody:string}, options?: any, downloadOnError?:boolean}) => {

    const [firstPlay, setFirstPlay] = useState<boolean>(true);
    const [cannotPlay, setCannotPlay] = useState<boolean>(false)

    if (!videoUrl){
        return null
    }
   
    return (
        <div className={`widget-video ${options.className}`} {...options}>
            {cannotPlay
            ? <div>
                <h3>{cannotPlayError?.errorTitle}</h3>
                <p>{cannotPlayError?.errorBody}</p>
                {downloadOnError && <a href='videoUrl' download>Download</a>}
            </div>
            : <video poster={featuredImageUrl} 
               onPlaying={() => {
                if(firstPlay) {
                    firstPlayEvent?.();
                    setFirstPlay(false);
                }
                }}
                controls 
                playsInline
            >
                <source src={videoUrl} onError={() => setCannotPlay(true)}/>          
            </video>}
  
            {(caption || description ) && 
                <div className="widget-caption">
                    {caption || description}
                </div>
            }
        </div>
    );
};

export default Video;